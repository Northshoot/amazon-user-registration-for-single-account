from django import forms
from models import Profile, Course

from registration.forms import RegistrationForm

class ProfileForm(forms.Form):
    course = forms.ModelChoiceField(queryset=Course.objects, label=u'Course')
    
    def save(self, user):
        try:	
            data = user.get_profile()
        except:
            data = Profile(user=user)
        
        data.course = self.cleaned_data["course"]
        data.save()
        
class UserRegistrationForm(RegistrationForm):
    course = forms.ModelChoiceField(queryset=Course.objects, label=u'course', empty_label=u'Empty')