'''
Created on Jul 19, 2012

@author: lauril
'''
from django.contrib import admin
from models import Course, Profile,AwsCredential

class ProfileAdmin(admin.ModelAdmin):
    list_display = ('user','course','created','updated_local','updated_amazon')
    list_filter = ('updated_local',)
    search_fields =('course','name','user')
    
admin.site.register(Course)
admin.site.register(Profile, ProfileAdmin)
admin.site.register(AwsCredential)
