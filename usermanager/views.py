# Create your views here.
from django.shortcuts import render_to_response
from django.template import RequestContext


def home(request):
    return render_to_response('content.html',dict(user=request.user),RequestContext(request))

def testing_url(request):
    from django.http import HttpResponse
    from cloud.crawler import cleanInstances
    cleanInstances()
    return HttpResponse('Cleaned for your pleasure')


