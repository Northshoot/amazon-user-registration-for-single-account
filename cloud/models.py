

from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from dateutil import parser
from django.utils.timezone import utc
# Create your models here.


from django.contrib.auth.models import User
from datetime import datetime


	
#class representing instance    
class Instance(models.Model):
	def __init__(self,*args,**kwargs):
		if not args:
			kwargs = self.cleanKwargs( kwargs )
		super(Instance, self).__init__(*args, **kwargs)
		
	_in_monitoring_element=models.NullBooleanField()
	ami_launch_index= models.CharField(max_length=40,null=True, blank=True)
	architecture= models.CharField(max_length=40,null=True, blank=True)
	#tags= models.BooleanField()
	created = models.BooleanField()
	client_token= models.CharField(max_length=40,null=True, blank=True)
	connection= models.CharField(max_length=40,null=True, blank=True)
	deleted_time = models.DateTimeField(null=True, blank=True)
	dns_name= models.CharField(max_length=40,null=True, blank=True)
	eventsSet= models.CharField(max_length=40,null=True, blank=True)
	group_name= models.CharField(max_length=40,null=True, blank=True)
	#groups= models.CharField(max_length=40)
	hypervisor= models.CharField(max_length=40,null=True, blank=True)
	instance_id= models.CharField(max_length=40,null=True, blank=True)
	image_id= models.CharField(max_length=40,null=True, blank=True)
	instanceState= models.CharField(max_length=40,null=True, blank=True)
	instance_type= models.CharField(max_length=40,null=True, blank=True)
	#interfaces= models.CharField(max_length=40,null=True, blank=True)
	ip_address= models.CharField(max_length=40,null=True, blank=True)
	item= models.CharField(max_length=40,null=True, blank=True)
	kernel= models.CharField(max_length=40,null=True, blank=True)
	key_name= models.CharField(max_length=40,null=True, blank=True)
	launch_time= models.CharField(max_length=40,null=True, blank=True)
	monitor_state= models.CharField(max_length=40,null=True, blank=True)
	monitored= models.NullBooleanField()
	networkInterfaceSet= models.CharField(max_length=40,null=True, blank=True)
	persistent= models.CharField(max_length=40,null=True, blank=True)
	placement= models.CharField(max_length=40,null=True, blank=True)
	platform= models.CharField(max_length=40,null=True, blank=True)
	previous_state= models.CharField(max_length=40,null=True, blank=True)
	private_dns_name= models.CharField(max_length=40,null=True, blank=True)
	private_ip_address= models.CharField(max_length=40,null=True, blank=True)
	product_codes= models.CharField(max_length=40,null=True, blank=True)
	public_dns_name= models.CharField(max_length=40,null=True, blank=True)
	ramdisk= models.CharField(max_length=40,null=True, blank=True)
	reason= models.CharField(max_length=40,null=True, blank=True)
	region= models.CharField(max_length=40,null=True, blank=True)
	requester_id= models.CharField(max_length=40,null=True, blank=True)
	root_device_name= models.CharField(max_length=40,null=True, blank=True)
	root_device_type= models.CharField(max_length=40,null=True, blank=True)
	shutdown_state= models.CharField(max_length=40,null=True, blank=True)
	spot_instance_request_id= models.CharField(max_length=40,null=True, blank=True)
	state= models.CharField(max_length=40,null=True, blank=True)
	state_code= models.IntegerField(null=True, blank=True)
	message= models.CharField(max_length=40,null=True, blank=True)
	subnet_id= models.CharField(max_length=40,null=True, blank=True)
	tenancy= models.CharField(max_length=40,null=True, blank=True)
	virtualization_type= models.CharField(max_length=40,null=True, blank=True)
	vpc_id= models.CharField(max_length=40,null=True, blank=True)
	
	
	def __unicode__(self):
		return "ID: %s created on %s with image %s" % (self.instance_id,self.launch_time, self.image_id)
	
		
	def instance_deleted(self):
		self.deleted_time = datetime.utcnow().replace(tzinfo=utc)
		
	def cleanKwargs( self,kwargs ):
		#for now just remove, can be replaced with models if needed
		to_pop=[ 'block_device_mapping','groups',  'state_reason','interfaces','instance_profile',
						'ebs_optimized', '_previous_state','tags', 'monitoring', 'monitoring_state',
						'sourceDestCheck']
		#id is busy by default django, changing to other var
		try:
			kwargs['instance_id']=kwargs.pop('id') 
			kwargs['instanceState']=kwargs.pop('_state')
			kwargs['placement']=kwargs.pop('_placement')
			#fix datate
			kwargs['launch_time']=parser.parse(kwargs.pop('launch_time'))
			kwargs['created']=True
			kwargs['region']=str(kwargs.pop('region')).split(':')[1]
		except Exception:
			pass
		for p in to_pop:
			try:
				kwargs.pop(p)
			except Exception:
				pass
		return kwargs
	
class InstanceCleaned(models.Model):
	instance = models.ForeignKey(Instance)
	reason = models.CharField(max_length=200)
	action = models.CharField(max_length=20)
	date =  models.DateField(auto_now_add=True)

	
	def __unicode__(self):
		return "%s on %s was %s, REASON: %s" %(self.instance.instance_id,
																		self.date, self.action, self.reason)


class CleanedEBS(models.Model):
	ebs_id =models.CharField(max_length=40,null=True, blank=True)
	reason = models.CharField(max_length=200,null=True, blank=True)
	deleted_on = models.DateTimeField(auto_now_add=True)
	
	def __unicode__(self):
		return "Deleted: %s on %s, REASON: %s"  %(self.ebs_id,
																		self.deleted_on, self.reason)


