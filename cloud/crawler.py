'''
Created on Jul 28, 2012

@author: lauril
'''
import boto, sys
from django.contrib.auth.models import User
from django.conf import settings
from boto.ec2.connection import EC2Connection 
import boto.ec2.autoscale
from boto.ec2.autoscale import AutoScaleConnection
from operator import itemgetter
import datetime
from dateutil import parser
import pytz
#from cloud.models import *
from cloud.models import InstanceCleaned, Instance
#from cloud.amazonses import AmazonSender

#always use secure
IS_SECURE = True
HOST='iam.amazonaws.com'
#allowed regions
allowed_regions =['eu-west-1','eu-west-1a','eu-west-1b','eu-west-1c','eu-west-1d']
superusers=['lauril', 'eao']
#add tags if necessary
globaltags =(u'user',u'Name')
#max runtime
max_run_time = 1

class BadInstanceConfiguration(Exception): 
    def __init__(self, reason, action, resource):
        super(BadInstanceConfiguration,self)
        self.states =['running', 'pending', 'stopping' , 'stopped']
        self.reason = reason
        self.action = action
        self.resource = resource
        
    def cleanResource(self):
        if self.action == "terminate" and self.resource.state in self.states:
            self.deleteInstance()
        elif self.action == "stop" and self.resource.state is not 'stopped' or 'stopping':
            self.stopInstance()
        else:
            print "Unknown command %s" %self.action
            
    def deleteInstance(self):
        try:
            self.resource.modify_attribute('disableApiTermination','false')
            self.resource.terminate()
            instance = Instance(**self.resource.__dict__)
            instance.instance_deleted()
            instance.save()
            d= InstanceCleaned.objects.create(instance = instance,
                                                                reason = self.reason,
                                                                action='terminated')
            d.save()
        except Exception as e:
            self.resource.modify_attribute('disableApiTermination','false')
            print "ERROR in deleteInstance: %s" %e
            d= InstanceCleaned.objects.create(instance = instance,
                                                                reason = "!!! Error stopping instance %s" %e,
                                                                action='stopped')
            d.save()
        
    def stopInstance(self):
        try:
            self.resource.stop()
            instance = Instance(**self.resource.__dict__)
            instance.save()
            d= InstanceCleaned.objects.create(instance = instance,
                                                                reason = self.reason,
                                                                action='stopped')
            d.save()
        except Exception as e:
            #self.resource.modify_attribute('disableApiTermination','false')
            print "ERROR in stopInstance: %s" %e
            d= InstanceCleaned.objects.create(instance = instance,
                                                                reason = "!!! Error stopping instance %s" %e,
                                                                action='stopped')
            d.save()
    
    def __str__(self):
        return "%s:::::%s::%s" %(repr(self), self.reason, self.action)
    
def cleanEBS():
    from cloud.models import CleanedEBS
    def_conn=boto.connect_ec2()
    regions = def_conn.get_all_regions()
    def_conn.close()
    for reg in regions:
        conn = EC2Connection(region=reg)
        vol = conn.get_all_volumes()
        for unattachedvol in vol:
            if unattachedvol.status == 'available':
                if not frozenset(globaltags).intersection(unattachedvol.tags.keys()):
                    debs = CleanedEBS.objects.create(ebs_id=unattachedvol.id,
                                      reason="Status %s without RQ tags %s" %(unattachedvol.status,
                                                                           str(unattachedvol.tags)))
                    debs.save()
                    unattachedvol.delete()
                else:
                    print unattachedvol.id,"\t", unattachedvol.status, "..have tags ", unattachedvol.tags
                #unattachedvol.delete()
            else:
                print unattachedvol.id,"\t", unattachedvol.status, "... not deleted"
                print "tags: " ,unattachedvol.tags

        conn.close()
#returns all autoscaling goups
#can be further integrated to clean them and create rules
def allAutoscale():
    all_autoscale = []
    def_conn=boto.connect_ec2()
    regions = def_conn.get_all_regions()
    def_conn.close()
    for reg in regions:
        conn = boto.ec2.autoscale.connect_to_region(reg.name)
        groups = conn.get_all_groups()
        for i in groups:
            all_autoscale.append(i)
        conn.close() 
    return all_autoscale

def allInstances():
    all_instances = []
    def_conn=boto.connect_ec2()
    regions = def_conn.get_all_regions()
    for reg in regions:
        conn = EC2Connection(region=reg)
        reservations = conn.get_all_instances()
        instances = [i for r in reservations for i in r.instances]
        for i in instances:
            all_instances.append(i)
        conn.close()
    return all_instances



def cleanInstances(instances=None, tags=None):
    if instances is None:
        instance = allInstances()
    if tags is None:
        tags = globaltags
         
    for i in instance:
        if str(i.__dict__['tags']['user']) not in superusers:
            try:
                #check if in allowed region
                if str(i.__dict__['region']).split(':')[1] not in allowed_regions:
                    raise BadInstanceConfiguration("Region %s is not allowed." %i.__dict__['region'] 
                                                   , "terminate",i)
                #all keys have to exist
                if not frozenset(globaltags).intersection(i.tags.keys()) :
                    raise BadInstanceConfiguration("No tags attached." , "terminate",i)
                #values of tags are not set
                if not len(tags) == len( i.__dict__['tags'].values()):
                    raise BadInstanceConfiguration("Tag values are empty: %s." %str(i.__dict__['tags'])
                                                   , "terminate",i)
                #no such user in database, assuming user tag mandatory
                try:
                    User.objects.get(username=i.__dict__['tags']['user'])
                except User.DoesNotExist,e:
                    raise BadInstanceConfiguration("No such user in database: %s"  %i.__dict__['tags']['user'],
                                                         "terminate",i)
                #check for max allowed run time
                if i.update() == 'running':
                    starttime = parser.parse(i.__dict__['launch_time'])
                    runtime = pytz.utc.localize(datetime.datetime.now()) - \
                                            starttime
                    from datetime import timedelta
                    if runtime > timedelta(hours=10):
                        raise BadInstanceConfiguration("Runtime for %s limit exceeded %s"  %(i.__dict__['tags']['user'],runtime),
                                                                "stop",i)

            except BadInstanceConfiguration as e:
                    e.cleanResource()
        else:
            print "Skipng super user"

    


