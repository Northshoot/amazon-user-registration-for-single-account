'''
Created on Jul 19, 2012

@author: lauril
'''
from boto.iam import connection
from boto import ec2
from boto.exception import *
from dateutil import parser
from cloud.crawler import * 
import sys

def getAwsInfo():
    data={}
    error = None
    err_pre = None
    conn= None
    try:
        conn = createConnetctionIAM()
        data['account_summ'] = conn.get_account_summary()
        data['all_users'] = conn.get_all_users()   
        instances = allInstances()
        inst=[]
        for i in instances:
            inst.append(i.__dict__)
        data['instances']=inst
    except Exception, e:
        #TODO: proper error checking is needed
        error = str(e)     
        print >> sys.stderr, "getAwsInfo(): %s" %e
    finally:
        conn.close()        
    return error, data
    
def createUser(user_name, path,group_name):
    data={}
    error = None
    try:
        conn = createConnetctionIAM()    
        data['user_create_response']=conn.create_user(user_name, path)['create_user_response']['create_user_result']['user']
        data['acc_keys'] = conn.create_access_key(user_name)['create_access_key_response']['create_access_key_result']['access_key']
        data['acc_keys']['aws_psw']=createPassWord(str(user_name),str(data['acc_keys']['access_key_id']) +str(data['acc_keys']['secret_access_key']))
        conn.create_login_profile(user_name, data['acc_keys']['aws_psw'])
        data['group'] = conn.add_user_to_group(group_name, user_name)    
    except Exception, e:
        #TODO: proper error checking is needed
        print >> sys.stderr, "createUser(): %s" %e
        error = e
        
    finally:
        conn.close()
        
    return error, data

def ceateKeys(user_name):
    pass

def updateKeys(user_name):
    pass 

def createPassWord(user_name, password):
    import random
    import hashlib
    l = list(user_name+password)
    random.shuffle(l)
    result = ''.join(l)
    s=hashlib.sha256()
    s.update(result)
    return s.hexdigest()[2:15]

def getUserDetails(user_name):
    """
    Collect user information and filters out interesting data
    Remember that secret key is shown only on creation of keys
    This is stored in local db
    #TODO: need synchronius algorithm for storing it
    Returns dictionary of 
    'aws_user' - dictionary
    'aws_keys' - list of dictionaries
    
    """
    data = {}
    error = None
    conn = None
    try:
        conn = createConnetctionIAM()
        data = {}
        data['aws_user'] = conn.get_user(user_name)['get_user_response']['get_user_result']['user']
        data['aws_keys'] = conn.get_all_access_keys(user_name)['list_access_keys_response']['list_access_keys_result']['access_key_metadata']
     
        conn.close()
        #we need to fix date fields
        date = parser.parse(data['aws_user']['create_date'])
        data['aws_user']['create_date']=date
        for d in data['aws_keys']:
            date = parser.parse(d['create_date'])
            d['create_date']=date
    except Exception, e:
        #TODO: proper error checking is needed
        print >> sys.stderr, "getUserDetails(user: %s): %s" %(user_name,e)
        error = e
    finally:
        conn.close()         
    return error, data

def getUserActivity(user_name):
    error = None
    conn = None
    data = None
    try:
        conn = createConnetctionIAM()
        data = conn.get_user(user_name)
    except Exception, e:
        #TODO: proper error checking is needed
        error = e
        print >> sys.stderr, "getUserActivity(user: %s): %s" %(user_name,e)
    finally:
        conn.close()         
    return error, data


def getAwsActivity():
    pass

def createConnectionEC2():
    return ec2.connection.EC2Connection()

def createConnetctionIAM():    
    return connection.IAMConnection()
