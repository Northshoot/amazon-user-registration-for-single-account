'''
Created on Mar 20, 2014

@author: lauril
'''
from __future__ import absolute_import

from celery import shared_task
from cloud.crawler import cleanEBS, cleanInstances



@shared_task
def cleanEBS():
    cleanEBS()
    
@shared_task
def cInstances():
    cleanInstances()