from django.contrib import admin
from models import InstanceCleaned, CleanedEBS, Instance

class InstanceAdmin(admin.ModelAdmin):
    #list_display = ('course','name')
    list_filter = ('instanceState','region','state')
    search_fields =('course','name')


admin.site.register(Instance,InstanceAdmin)
admin.site.register(InstanceCleaned)
admin.site.register(CleanedEBS)
