'''
Created on Jul 19, 2012

@author: lauril
'''
from views import createUserAWS,viewUserAWS,viewActivityAWS, getAWSInfo,help
from django.conf.urls import patterns, url


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'StudenManager.views.home', name='home'),
    url(r'createUserAWS/$', createUserAWS),
    url(r'viewUserAWS/(?P<u_rq>\w{3,6}\-\d{1}|\w{3,6})/$',viewUserAWS),
    url(r'viewUserAWS/$',viewUserAWS),
    url(r'getAWSInfo/$',getAWSInfo),
    url(r'viewActivityAWS/$', viewActivityAWS),
    url(r'help/$',help),
    #url(r'$',redirect_to,{'url':'/user/'}),


    
)
