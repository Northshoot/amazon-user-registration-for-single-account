# Create your views here.
'''
OBS! All views here have to be decorated with
@login_required
'''
from django.shortcuts import render_to_response
from django.http import HttpResponse,HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from cloud.awsmanager import *
from django.template import RequestContext, Context
from django.contrib.auth.models import User
from usermanager.models import Profile, AwsCredential
from datetime import datetime

from django.utils.translation import ugettext_lazy as _
from django.contrib import messages

import sys

        
@login_required
def getAWSInfo(request):
    if request.user.is_staff:
        error, data = getAwsInfo()
        #extract only users from response
        before = datetime.now()
        if not error:
            try:
                f_users = data['all_users']['list_users_response']['list_users_result']['users']
            except KeyError,e :
                print >> sys.stderr, "getAWSInfo %s" % e
                return HttpResponse("KeyError %s" %e)
            exectime= datetime.now()-before
            #the returned date in the dict is a string, we convert here into pythons object it is
            #possible to format it in template
            for u in f_users:
                date = parser.parse(u['create_date'])
                u['create_date'] = date
            return render_to_response( 'cloud/viewAWS.html',Context({'info':error,'aws_users':f_users, 'instance_keys':data['instances'][0].keys() ,'instances':data['instances'],"time":exectime.seconds}),RequestContext(request))
        else:
            return HttpResponse("Error getting data %s" %(error))
        
    else:
        return HttpResponse("getAWS")
    
@login_required
def createUserAWS(request):
    """
    User is created here, after that we forward him to viewUsersAWS/use_name/
    
    """
    error = None
    try:
        user = User.objects.get(username=request.user)
    except User.DoesNotExist,e :
        error = "Could not find user: %s" %str(e)
        print >>sys.stderr, error
        
    try:    
        profile = Profile.objects.get(user=user)
    except Profile.DoesNotExist,e :
        error = "Could not find profile for user: %s, error: %s" %(user,str(e))
        print >>sys.stderr, error
        
    if error:
         return HttpResponse("Serios Error has occure, contact laurynas.riliskis@ltu.se and copy %s"%error)   
    if not profile.isAws():
        c= profile.getCourse()
        #request.user must go into create user 
        error, data = createUser(request.user,"/students/"+c+"/",c)
        if error :
            try:
                msg=_("Error code: %s \n") %str(error) # %(error.__getattr__("code"),error.__getattr__("message"))
                print >> sys.stderr, "Error creating user %s" % str(error)
            except Exception, e:
                msg=_("Error getting error code %s") %str(e)
                print >> sys.stderr, "Error Creating user %s" %str(e)
    
            messages.error(request,msg)
            return HttpResponseRedirect('viewUserAWS/')
        if data:  
            #create AwsCredential pair
            obj = AwsCredential.objects.create(user=user, 
                    access_key_id=data['acc_keys']['access_key_id'], 
                    secret_access_key=data['acc_keys']['secret_access_key'],
                    created=parser.parse(data['acc_keys']['create_date']),
                    aws_user_id=data['user_create_response']['user_id'],
                    arn=data['user_create_response']['arn']
                    )
            obj.save()
            profile.password = data['acc_keys']['aws_psw']
            profile.awsCreated()
            profile.save()
            msg=_("Account created successfully")
            messages.success(request,msg)
            return HttpResponseRedirect('viewUserAWS/')
    else:
        msg=_("You already have created AWS account")
        messages.success(request,msg)
            
    return HttpResponseRedirect('viewUserAWS/')

    
@login_required
def viewUserAWS(request, u_rq=None):
    #allow only admins look up others
    if request.user != u_rq or not request.user.is_staff:
        u_rq = request.user

    #get AWS user data
    t_start = datetime.now()
    error, data=getUserDetails(u_rq)
    exectime= datetime.now()-t_start
    data['local_user']=User.objects.get(username=u_rq)
    data['local_profile']=Profile.objects.get(user=data['local_user'])
    if error:
        try:
            msg="Error code: %s - Message: %s" %(str(error.__getattr__("code")),str(error.__getattr__("message")))
        except Exception, e:
            print >> sys.stderr, "Error: %s" %str(e)
            msg=_("Error processing request: %s")  %str(e)	
        messages.error(request,msg)
    else:    
        for d in data['aws_keys']:
            try:
                key=AwsCredential.objects.get(access_key_id=d['access_key_id'])
                d['secret_key_id']=key.secret_access_key
            except AwsCredential.DoesNotExist,e:
                print >> sys.stderr, "AwsCredential.DoesNotExist: %s" %str(e)
                d['secret_key_id']="Unknown"
                msg=_("No AWS user found, create one to start using Amazon Cloud service.")
                messages.error(request,msg)

    return render_to_response('cloud/viewUser.html',
            Context({'u_data':data, "time":exectime.seconds}),
            RequestContext(request))


@login_required
def viewActivityAWS(request):
    from cloud.tasks import pprintInstances
    r = pprintInstances.delay()
    return HttpResponse(r.result)

@login_required
def help(request):
    if request.user.is_staff:
        return HttpResponse("Admin help")
    else:
        return HttpResponse("<h1>Help</h1><br/>FAQ will be published here as the course progress. ")
