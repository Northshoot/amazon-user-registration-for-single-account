# This is my README
What to configure
I run whit app from bitnami 12.04 64bit Ubuntu on Amazon AWS
Prior the running following have been installed:

sudo apt-get install git
sudo apt-get install python-pip python-dev build-essential 
sudo pip install -U boto
sudo pip install django-registration
sudo pip install django-celery
more info on celery http://celery.readthedocs.org/en/latest/django/first-steps-with-django.html




settings.py
#system uses djangos emailing with amazon aws SES, you will need to enable it in your controll panel or use different mailing backend.
AUTH_PROFILE_MODULE = 'usersmanager.Profile'
ACCOUNT_ACTIVATION_DAYS=3
REGISTRATION_OPEN = True

AWS_SMTP_USER = ''
AWS_SMTP_PSW = ''

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

LOCAL_HOST='ltu.se'
EMAIL_HOST='email-smtp.eu-west-1.amazonaws.com'
DEFAULT_FROM_EMAIL=''
EMAIL_USE_TLS=True
EMAIL_PORT=587
EMAIL_HOST_USER=''
EMAIL_HOST_PASSWORD=''

templates\registration
activate.html - the redirect url
activation_email.txt - the url to the activation

Resources
on registration
http://www.michelepasin.org/techblog/2011/01/14/setting-up-django-registration/

you will need to install both Celery and Rabbit

run rabbit
$ sudo rabbitmq-server

you can also run it in the background by adding the -detached option (note: only one dash):

$ sudo rabbitmq-server -detached

Never use kill to stop the RabbitMQ server, but rather use the rabbitmqctl command:

$ sudo rabbitmqctl stop


